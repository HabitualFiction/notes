Monitoring
==========

ELK Stack
~~~~~~~~~
https://www.docker.elastic.co/


.. code-block:: console

  docker export "$(docker create --name metricbeat docker.elastic.co/beats/metricbeat)" -o metricbeat.tar
