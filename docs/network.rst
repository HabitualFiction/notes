Network
=======

Tools
~~~~~

ip
--

ss
--

nmap
----

nping
-----

tcpdump
-

traceroute
----------

iperf
-----




Troubleshooting
~~~~~~~~~~~~~~~



ICMP Redirects
--------------

RCVD (22.8873s) ICMP [10.205.2.1 > 10.205.2.150 Network redirect (type=5/code=0) addr=10.205.2.254] IP [ttl=255 id=16228 iplen=56 ]


Performance
~~~~~~~~~~~

Check for dns lookups for known viruses and malformed requests in general
