Linux
=====


Shells
~~~~~~
zsh_

fish_

bash_


.. _fish: https://wiki.archlinux.org/index.php/Fish
.. _zsh: https://wiki.archlinux.org/index.php/zsh
.. _bash: https://wiki.archlinux.org/index.php/bash

zsh
---
Here are the contents of my ``~.zshrc``

.. code-block:: bash

  source /etc/profile

  export ZSH=/home/cdraper/.oh-my-zsh
  source $ZSH/oh-my-zsh.sh

  ZSH_THEME="powerlevel9k/powerlevel9k"
  POWERLEVEL9K_MODE='nerdfont-complete'


  plugins=(kubectl celery cp colored-man-pages gcloud-zsh-completion docker-compose docker-machine dnf fabric fedora nmap rsync screen ssh-agent tmux sudo git adb docker git-extras vagrant pip sudo gem firewalld rake bundler)


  alias bpython2="python2 -m bpython"
  alias namebench="namebench.py"
  alias R="echo your port is 57600 dumbass;sudo -E ssh -l $USER -F ~/.ssh/config -L 443:localhost:57600 cdraper@remote-support.scalecomputing.com"
  alias RS="ssh cdraper@remote-support.scalecomputing.com"
  alias RL="ssh cdraper@remote-support.scalecomputing.local"
  alias RR="echo your port is 57600 dumbass;ssh -L 57600:localhost:57600 remote-support.scalecomputing.com"
  alias addkey="ssh-add ~/.ssh/scale/*"
  alias pipupdate="su -c 'pip freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U'"
  alias pip3update="su -c 'pip3 freeze --local | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip3 install -U --user --force-reinstall'"
  alias redirectmonit="ssh -f cdraper@remote-support.scalecomputing.com -L 50000:localhost:65000"
  alias sc="ssh cluster -p "
  alias docker-remove-images="docker rmi $(docker images -q)"
  alias docker-remove-containers="docker rm $(docker ps -a -q)"
  alias gitgrep="grep -I -rnw '/home/cdraper/Git' -ie "
  alias g="googler -n 7 -c us -l en"
  alias snsmount="sshfs root@10.205.109.124:/ /home/cdraper/TestCluster -C"

  alias s-ready="/home/cdraper/Git/scale-smoothstone/smoothstone.py --ready"
  alias s-work="/home/cdraper/Git/scale-smoothstone/smoothstone.py --work"
  alias s-logout="/home/cdraper/Git/scale-smoothstone/smoothstone.py --logout"
  alias s-break="/home/cdraper/Git/scale-smoothstone/smoothstone.py --takebreak"

  function thrift-session() { python2 -m bpython -i /home/cdraper/Git/scale-thrift/scaled-api/client.py --host $1 --username $2 --password $3 ;} # 1 IP 2 USERNAME 3 PASS

  function RR2() { ssh cluster -p $1 "sc configdata show type scaled::User && sc condition show display set" && ssh -L 57600:localhost:57600 cdraper@remote-support.scalecomputing.com ssh -L57600:localhost:443 root@localhost -p $1 ;}
  function RR3() { ssh cluster -p $1 "sc configdata show type scaled::User && sc condition show display set" && sudo -E ssh -L 443:localhost:57600 cdraper@remote-support.scalecomputing.com ssh -L 57600:localhost:443 root@localhost -p $1 ;}
  function RR4() { sudo -E ssh -L 443:localhost:57600 cdraper@remote-support.scalecomputing.com ssh -L 57600:localhost:443 root@localhost -p $1 ;}
  function RR5() { ssh cluster -p $1 "sc configdata show type scaled::User && sc condition show display set" && sudo -E ssh -L 57601:localhost:57601 cdraper@remote-support.scalecomputing.com ssh -L 57601:localhost:443 root@localhost -p $1 ;}
  function proxyconnect() { ssh -D 57600 -p $1 cluster ;} # not working
  function sc-dl() { scp -C -P $1 cluster:$2 $3 ;} # where 1 is the case 2 is remote file and 3 is local file
  function sc-dldir() { scp -r -C -P $1 cluster:$2 $3 ;}
  function sc-upload() { scp -C -P $1 $2 cluster:$3 ;} # where 1 is case 2 is local file and 3 is remote file
  function sc-vnc() { ssh -C -L 5900:localhost:57600 cdraper@remote-support.scalecomputing.com -o RemoteCommand="ssh -p $1 -L 57600:localhost:$2 root@localhost -N -f" -f && vncviewer :0 ;} # $1=case port $2=remote vnc port
  function sc-cases() { RS tlns | grep $1 }

  if [ $commands[kubectl] ]; then
  fi

  if ! pgrep -u "$USER" ssh-agent > /dev/null; then
  fi
  if [[ "$SSH_AGENT_PID" == "" ]]; then
  fi

  autoload -Uz compinit compdef promptinit
  compinit
  promptinit
  zstyle ':completion:*' menu select
  setopt COMPLETE_ALIASES

  source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  zstyle ':completion:*' rehash true




SSH config
~~~~~~~~~~

.. code-block:: bash

  Host *
    Compression yes
    StrictHostKeyChecking no
    TCPKeepAlive yes
    ServerAliveInterval 100
    GSSAPIAuthentication no
    PubkeyAcceptedKeyTypes +ssh-dss

  Host cluster
    ProxyJump cdraper@remote-support.scalecomputing.com
    HostName localhost
    User root
    IdentitiesOnly yes
    IdentityFile ~/.ssh/scale/scale_computing2016_id_rsa
    IdentityFile ~/.ssh/scale/scale_computing2012_id_dsa
    IdentityFile ~/.ssh/scale/scale_computing_id_dsa
    ForwardAgent no
    CheckHostIP no
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
    LogLevel ERROR

  Host remote-support.scalecomputing.com rs remote-support remote-support.scalecomputing.local
    Hostname remote-support.scalecomputing.com
    User cdraper
    IdentityFile ~/.ssh/id_rsa
    IdentityFile ~/.ssh/scale/scale_computing2016_id_rsa
    IdentityFile ~/.ssh/scale/scale_computing2012_id_dsa
    ForwardAgent yes
    AddKeysToAgent yes
    StrictHostKeyChecking yes
    IdentitiesOnly yes
    GSSAPIAuthentication no


Mac OS X
========


Windows
=======
