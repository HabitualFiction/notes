Locked terminal on accident
~~~~~~~~~~~~~~~~~~~~~~~~~~~

S is so near to A, and we all use ``Ctrl-A`` all the time. So, it is easy to hit ``Ctrl-S`` by accident.

``Ctrl-S`` pauses flow-control (XOFF), the terminal will accept inputs but will not display output.

To tart flow-control, simply give ``Ctrl-Q`` (XON).
