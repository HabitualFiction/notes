Lenovo NE1032
=============

This section assumes you've familiarized yourself with the General networking section.

Basic Configuration
~~~~~~~~~~~~~~~~~~~


Update the firmware
-------------------


.. note:: This guide was written for version "10.7.2.0" you will need to update if on a lesser version due to command syntax changes.

Set the management IP
---------------------

The management network should be separated from any data networks and have physically independent hardware supporting it. The goal of the management network is to maintain remote access to network equipment should a configuration change interrupt the data networks.

.. code-block:: console

  > en

  # conf t

  (config)# int mgmt0

  (config)# ip addr 192.168.1.2 255.255.255.0


Set the default route for the management network
------------------------------------------------

.. code-block:: console

  (config)# vrf context management

  (config-vrf)# ip route 0.0.0.0/0 10.205.13.1


Generate new SSH keys
---------------------

.. code-block:: console

  # conf t
  (config)# no feature ssh
  (config)# ssh key rsa length 2048 force
  (config)# ssh login-attempts 3
  (config)# feature ssh


Create additional users
-----------------------

.. note:: The `network-admin` has full administrative rights.
.. note:: The `network-operator` is a limited account.

.. code-block:: console

  (config)# username someadmin role network-admin password somepass

  (config)# username someuser role network-operator password somepass


Secure mode
-----------
Secure mode will disable insecure protocols and ciphers. For example, telnet is disabled and SSH will be forced to use a secure cipher algorithm such as ECDHE-RSA-AES256-SHA384.


.. code-block:: console

  (config)# secure mode enable
  (config)# show security mode


Set NTP
-------

.. code-block:: console

  (config)# feature ntp
  (config)# ntp server 10.205.13.2

Set DNS
-------

.. code-block:: console

  (config)# ip name-server 10.205.13.3 vrf default

Check spanning tree
-------------------

.. code-block:: console

  (config)# sh spanning-tree

Ensure that the spanning tree protocol supports `rapid` convergence.

.. code-block:: console

    VLAN0001
    spanning-tree enabled protocol rapid-pvst
    ROOT ID      priority    32769
                 address     a48c.db96.7f00
                 This bridge is the root
                 Hello Time  2   Max age 20  Forward Delay 15

    BRIDGE ID    priority    32769 (32768 sys-id-ext 1)
                 address     a48c.db96.7f00
                 Hello Time  2   Max age 20  Forward Delay 15

   Interface        Role Sts cost      Prio.Nbr      Type
   ---------------- ---- --- --------- ------------- ----------------------
   Ethernet1/1      Desg FWD 2          128.410001   point-to-point
   Ethernet1/2      Desg FWD 2          128.410002   point-to-point
   Ethernet1/3      Desg FWD 2          128.410003   point-to-point
   Ethernet1/4      Desg FWD 2          128.410004   point-to-point


Create VLANs
------------

.. code-block:: console

  (config)# vlan 1-10

Access Switchport Mode
----------------------

.. code-block:: console

  (config)# interface ethernet 1/1
  (config-if)# switchport mode access
  (config-if)# switchport access vlan 10

Hybrid Switchport Mode
----------------------

.. code-block:: console

  (config)# interface ethernet 1/1
  (config-if)# switchport mode hybrid
  (config-if)# switchport hybrid native vlan 5
  (config-if)# switchport hybrid allowed vlan 1-10
  (config-if)# switchport hybrid egress-tagged vlan 1,2,3,4,6,7,8,9,10
  (config-if)# show interface eth1/1 switchport
  (config-if)# show running-config interface ethernet1/1

.. note:: The native vlan is an ingress rule stating that any untagged traffic be placed in that vlan.
.. note:: The egress-tagged rule defines which vlans will leave the switchport tagged.


Trunk Switchport Mode
---------------------

.. code-block:: console

  (config)# interface ethernet 1/1
  (config-if)# switchport mode trunk
  (config-if)# switchport trunk allowed vlan all
  (config-if)# switchport trunk native vlan 5
  (config-if)# show interface eth1/1 switchport
  (config-if)# show running-config interface ethernet1/1

Link Layer Discovery Protocol
-----------------------------

.. code-block:: console

  (config)# interface ethernet 1/1
  (config-if)# lldp transmit
  (config-if)# lldp receive
  (config-if)# end
  # show lldp interface all


Logging
-------

.. code-block:: console

  (config)# logging logfile switch_log 7 size 10485760
  (config)# show logging logfile


Advanced Configuration
~~~~~~~~~~~~~~~~~~~~~~

Set the default route for the default network
---------------------------------------------

.. code-block:: console

  (config)# ip route 0.0.0.0/0 10.205.13.1

Telemetry
---------

.. code-block:: console

  (config)# feature telemetry
  (config)# telemetry controller ip 10.205.13.34 port 5001 vrf management
  (config)# telemetry heartbeat enabled interval 60


Traffic Mirroring
-----------------
The ERSPAN feature allows you to monitor traffic on one or more ports or more VLANs, and send the monitored traffic to one or more destination ports. ERSPAN sends traffic to a network analyzer, such as an IDS. ERSPAN supports "many to one" meaning that multiple ports can be monitored and sent to a single destination.

.. note:: only two ERSPAN sessions are allowed to run simultaneously on the switch

.. code-block:: console

  (config)# monitor erspan origin ip-address 10.205.13.252 global
  (config)# monitor session 1 type erspan-source
  (config-erspan-src)# vrf default
  (config-erspan-src)# source interface ethernet 1/4
  (config-erspan-src)# destination ip 10.205.13.34
  (config-erspan-src)# no shutdown


.. code-block:: console

  (config)# show monitor session 1
     session 1
     session 1
  ---------------
   type              : erspan-source
   state             : up
   vrf-name          : default
   destination-ip    : 10.205.13.34
   ip-ttl            : 255
   ip-dscp           : 0
   origin-ip         : 10.205.13.251  (global)
   source intf       :
       tx            : Ethernet1/4
       rx            : Ethernet1/4
       both          : Ethernet1/4
   source VLANs      :
       rx            :


Sampled Flow
------------
sFlow was designed for providing constant site-wide and enterprise-wide traffic monitoring. CNOS supports sFlow version 5.


Set the global configuration options for the collector and intervals:

.. code-block:: console

  (config)# feature sflow
  (config)# slow collector ip 10.205.13.36
  (config)# sflow polling-interval 60

Enable sFlow monitoring on a per port basis:

.. code-block:: console

  (config)# interface ethernet 1/1
  (config-if)# sflow enable

.. note:: the default port is 6343
.. note:: setting the polling interval to 0 will disable polling

Display statistics:

.. code-block:: console

  (config)# show sflow statistics

Clear statistics:

.. code-block:: console

  (config)# clear sflow statistics


Periodic samples of traffic data can be sent to the sFlow analyzer. The sampling rate can be set to send samples from every 4096 to 1000000000 packets.

.. code-block:: console

  (config)# sflow sampling-rate 4096
  (config)# sflow max-sampled-size 256

Troubleshooting
~~~~~~~~~~~~~~~

Ping
----
Ping is very flexible in CNOS. There are many options that can be used to test specific ports, etc.

Issuing ping with no options will start a wizard:

.. code-block:: console

  # ping
  Vrf context to use [default]: management
  Protocol [ip]:
  Target IP address or hostname: 8.8.8.8
  Repeat count [5]:
  Datagram size [56]:
  Timeout in seconds [2]:
  Sending interval in seconds [1]:
  Extended commands [n]:
  PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
  64 bytes from 8.8.8.8: icmp_seq=1 ttl=120 time=15.2 ms
  64 bytes from 8.8.8.8: icmp_seq=2 ttl=120 time=15.4 ms
  64 bytes from 8.8.8.8: icmp_seq=3 ttl=120 time=16.2 ms
  64 bytes from 8.8.8.8: icmp_seq=4 ttl=120 time=15.1 ms
  64 bytes from 8.8.8.8: icmp_seq=5 ttl=120 time=16.8 ms

  --- 8.8.8.8 ping statistics ---
  5 packets transmitted, 5 received, 0% packet loss, time 4005ms
  rtt min/avg/max/mdev = 15.152/15.783/16.800/0.647 ms

.. note:: Use the default VRF if testing from a VLAN (SVI) interface
