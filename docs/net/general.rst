General
========

Basics
~~~~~~

CLI Modes
---------

``>`` = unprivileged mode
``#`` = privileged mode
``(config)#`` = config mode

Topology
--------

Spanning Tree
-------------

Switched Virtual Interface
--------------------------

Virtual Routing and Forwarding
------------------------------
