**Storage**
===========

SAS
~~~

.. code-block:: bash

  badblocks -sv /dev/sda
	smartctl -x /dev/sda

Bootloader
~~~~~~~~~~

.. code-block:: bash

  file -sb /dev/sda

RAID
~~~~

Hardware (MegaRAID)
-------------------
https://www.maths.cam.ac.uk/computing/docs/public/megacli_raid_lsi.html
https://wikitech.wikimedia.org/wiki/MegaCli

.. code-block:: bash

  /opt/MegaRAID/MegaCli/MegaCli64 -AdpAllInfo -aAll
  /opt/MegaRAID/MegaCli/MegaCli64 -PDList -aALL
  /opt/MegaRAID/MegaCli/MegaCli64 -AdpPR -Info -aALL
  /opt/MegaRAID/MegaCli/MegaCli64 -AdpGetProp PatrolReadRate -aALL
  CfgDsply -aALL
  cd /tmp; ${megacli} -AdpEventLog -GetEvents -f events.log -aALL && tail -n 30 events.log
  -EncInfo -aALL
  -AdpBbuCmd -aALL
  -ShowSummary -aALL






Software (md)
-------------

.. code-block:: bash

	mdadm --examine /dev/sd[a-z]2 | egrep "Event|/dev/sd"

.. code-block:: bash

	mdadm --zero-superblock /dev/sda2
	mdadm --add /dev/sda2

Tiering
~~~~~~~

if your shit gets high on the SSDs, you have three options to fix them:

====

**-The Gentle Way:**
	Adjust the tiering.limiter from 1 to 2 per node, see how it fares.

**-The Agressive Way:**
	Force a retier recover.

**-The Nuclear Option:**
	FUCKING SET ALL THE THINGS TO PRIORITY_FACTOR < 0 > ...
		"FUCK THE SSDS AND GET THE FUCK OUT"


Performance
~~~~~~~~~~~


iostat
------

.. code-block:: console

  iostat -x 2 5

iotop
-----

.. code-block:: console

	iotop -o


top
---
Run top then press `F` and then `u` to sort by page faults.


.. code-block:: console

	top

sar
---

.. code-block:: console

	sar 1 7

vmstat
------

.. code-block:: console

	vmstat 1
