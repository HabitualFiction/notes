Seal Team Rick's Documentation
==============================

The main documentation for the site is organized into a couple sections:

* :ref:`hypervisor`
* :ref:`networking`
* :ref:`virtual machines`
* :ref:`containers`
* :ref:`engineer tools`
* :ref:`projects`
* :ref:`refs`

.. * :ref:`teams`

.. _hypervisor:
.. toctree::
   :maxdepth: 2
   :caption: Hypervisor

   network
   storage
   compute
   migration
   scripts

.. _networking:
.. toctree::
  :maxdepth: 2
  :caption: Networking

  net/general
  net/lenovo
  net/dell

.. _virtual machines:
.. toctree::
   :maxdepth: 2
   :caption: Virtual Machines

   vm/general

.. _containers:
.. toctree::
  :maxdepth: 2
  :caption: Containers

  docker


.. _engineer tools:
.. toctree::
  :maxdepth: 2
  :caption: Engineer Tools

  support
  applications

.. _projects:
.. toctree::
  :maxdepth: 2
  :caption: Projects

  projects

.. .. _teams:
.. .. toctree::
   :maxdepth: 2
   :caption: Teams

   teams


.. _refs:
.. toctree::
  :maxdepth: 2
  :caption: References

  resources
  junk/hello
  bash
