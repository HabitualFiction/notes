Links
=====



Linux Networking
~~~~~~~~~~~~~~~~
https://wiki.linuxfoundation.org/networking/start
http://linux-ip.net/html/tools-ip-route.html
https://baturin.org/docs/iproute2/

Bug Hunting
~~~~~~~~~~~

CentOS bug tracker
------------------
https://bugs.centos.org/print_all_bug_page.php

OpenStack
---------
https://bugs.launchpad.net/nova/

RHEL
----
https://bugzilla.redhat.com/

Automatic bug detection and reporting tool
------------------------------------------
https://github.com/abrt/abrt

Determine if there was a crash
------------------------------
https://access.redhat.com/articles/2642741

Unsorted
~~~~~~~~
https://kernelnewbies.org/

https://www.kernel.org/doc/Documentation/vfio.txt

https://raid.wiki.kernel.org/index.php/A_guide_to_mdadm

https://www.thomas-krenn.com/en/wiki/Partition_Alignment

https://taskwarrior.org/download/

https://www.server-world.info/en/

http://www.policyrouting.org/iproute2.doc.html

https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst
