Compute
=======

.. code-block:: bash

  mpstat -P ALL


.. code-block:: bash

  systemd-cgtop


Logging
~~~~~~~

/var/log/libvirt/libvirtd.log
/var/log/libvirt/qemu/$(VM_UUID).log
