Scripts
=======


Duplicate Address Detection
~~~~~~~~~~~~~~~~~~~~~~~~~~~
To run the script for the first time, you first need to make the script executable.
You do this by typing the following line in the shell.
  $ chmod +x <filename>
  OR
  $ chmod 764 <filename>

To run the file, type the following line:
  $ ./<filename>

.. note:: These were written with the intent to run on a cronjob and send email alerts

.. code-block:: bash

  #!/bin/bash

  lan_list=$(cat /opt/scale/var/nodes | awk '{ print $3 }')

  for lanIP in $lan_list
  do
    backplane=$(cat /opt/scale/var/nodes | grep $lanIP | awk '{ print $2}')
    ssh $backplane arping -D -c 1 -I lan $lanIP
    result=$(echo $?)
    if(( $result != 0 )); then
      $(echo "IP conflict detected at" $(date) >> conflictlog.txt)
    fi
  done

Email alerting is handled using the following:

.. code-block:: bash

  #!/bin/bash

  if [ -s conflictlog.txt ]; then
    $(echo -e "IP conflict detail: \n $(cat conflictlog.txt)" | mailx -s "IP conflict detected" rtsering@scalecomputing.com)
    $(rm conflictlog.txt)
  else
    echo "file does not exist"
  fi


VM Changerate
~~~~~~~~~~~~~
Note the time-stamp. Edit it to whatever time you want, then run the script. You can ONLY run this in the style "from X time to now". No "run from time A to time B".
Someone smarter than me could probably re-write it to do include such logic.

.. code-block:: bash

   #!/bin/bash

   sc vm show display snaps | gawk '
   /VM.GUID/ { if (d > 0) print vm,d; vm=$NF; d = 0; }
   /^           Timestamp/ { match($0, / : (.*)$/, m); ts=m[1]; }
   /^           Block Diff/ && ts >= "2018-03-14 14:00:00"  && $NF < 100000 { d += $NF }
   END { if (d > 0) print vm,d; }
   '
