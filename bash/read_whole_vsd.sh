#!/usr/bin/env bash
# this script forces an entire VSD to be read. This is one way to force a VSD to use all its tier 0 allocation

arg_str="$*"

[ -z "$1" ] && echo "USAGE: $0 complete_vsd_uuid [skip_read]" && exit 1

function given_arg {
	echo "$arg_str" | grep $1 &> /dev/null && return 0
	return 1
}

function cleanup {
	sc vsd show display heatmap uuid $vsd | grep -i pop
}
trap cleanup EXIT

vsd=$1

function update_heatmap {
	local trigger_pop=$(sc vsd show display heatmap uuid $vsd | grep triggerTier0Pop | awk '{print $NF}')
	sc vsdheatmap update uuid $vsd minTier0Pop $((trigger_pop - 1)) &> /dev/null
}

function read_vsd {
	local capacity_in_bytes=$(sc vsd show display detail uuid $vsd | grep CapacityBytes | awk '{print $NF}')

	scribecp -iv $vsd -of /dev/null -bs 1048576 -c $capacity_in_bytes &
	local scribecp_pid=$!

	echo "Reading entire VSD 1 block at a time..."
	while kill -0 $scribecp_pid; do
		printf "."
		update_heatmap
		sleep 20
	done
	echo Reads complete.
}

! given_arg skip_read && read_vsd

update_heatmap
echo "Waiting for Up moves to reach zero."
sleep 2
while ! sc vsd show display heatmap uuid $vsd | grep -E 'Up.Moves\s+: 0 '; do
	sleep 20
	printf "."
	update_heatmap
done