#!/usr/bin/env bash

sed 's/blah/&/'  # ampersand will place "blah" and that way you can wrap it in parens for instance
sed 's/blah/(&)/' # it just reprints the variable and modifies it as defined