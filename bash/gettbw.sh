#!/usr/bin/env bash
#
# Mike Lyon
# 2017
#
# # # # # #

nodeArray=()
declare -a diskArrayEight=(sdg sdh)

drives=`sc rsd show | grep -c LOCAL`
if (( $drives > 6 ));
        then
	for node in `cat /opt/scale/var/nodes | awk '{print $2}'`;
	do
	   ssh $node smartctl -A /dev/sdg | awk '/^241/ { print " /dev/sdg TBW: "($10 * 512) * 1.0e-12, "TB" } '
	   ssh $node smartctl -A /dev/sdh | awk '/^241/ { print " /dev/sdh TBW: "($10 * 512) * 1.0e-12, "TB" } '
	   echo " "
	done
        else
        for node in `cat /opt/scale/var/nodes | awk '{print $2}'`;
        do
     	   ssh $node smartctl -A /dev/sda | awk '/^241/ { print " /dev/sda TBW: "($10 * 512) * 1.0e-12, "TB" } '
           echo " "
        done
fi
