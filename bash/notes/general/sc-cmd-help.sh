#!/bin/bash
[root@scale-192-168-1-24 ~ 11:15:34 ]# sc help
Usage: sc [OPTIONS] SUBCOMMAND
OPTIONS:
  -l, --loglevel LEVEL                    Set log level to LEVEL
  -q, --quiet                             Only print bare minimum output
  --scaled-thrift-port PORT               Set this PORT for scaled thrift calls
  --scaled-private-thrift-port PORT       Set this PORT for scaled private thrift calls
  --scribed-thrift-port PORT              Set this PORT for scribed thrift calls

SUBCOMMAND:
   alertemailtarget create email <string> [guid <target guid>] [alertTagGUID <alert tag guid>] [resendDelay <int=0>] [silentPeriod <int=0>]
   alertemailtarget delete guid <target guid>
   alertemailtarget show [order <enum(guid|alertTag|email)=guid>]

   alertinstance create [transport <enum(smtp|syslog|all)=all>]
   alertinstance show [order <enum(id|state|targetGUID|created|lastAttempt)=id>]

   alertsmtpserver create host <string> [port <int=25>] [ssl <yes or no=no>] [auth <yes or no=no>] [user <string>] [passwd <string>]
   alertsmtpserver delete
   alertsmtpserver show
   alertsmtpserver update [host <string>] [port <int>] [ssl <yes or no>] [auth <yes or no>] [user <string>] [passwd <string>]

   alertsyslogtarget create host <string> [port <int=0>] [protocol <string(udp|tcp)=udp>] [guid <target guid>] [alertTagGUID <alert tag guid>] [resendDelay <int=0>] [silentPeriod <int=0>]
   alertsyslogtarget delete guid <target guid>
   alertsyslogtarget show [order <enum(guid|alertTag|hostname)=guid>]

   authenticationkey generate port <int> [cluster <yes or no=no>]
   authenticationkey delete port <int> [cluster <yes or no=no>] confirm <yes or no=no>
   authenticationkey read port <int> file <string> [cluster <yes or no=no>] [truncate <yes or no=yes>]
   authenticationkey accept port <int> file <string> [cluster <yes or no=no>]
   authenticationkey revoke file <string> [cluster <yes or no=no>]

   authenticationserver check ip <string> [port <int=0>] [servicePort <int=0>] [cluster <yes or no=no>]

   checkedvalue refresh [filter <string=.*>]
   checkedvalue set name <string> value <string>
   checkedvalue show [filter <string=.*>] [order <enum(name|value|flags|modified)=name>]

   cluster ping [count <int=1>] [timeout <int=1>] [interval <int=1>]
   cluster shutdown [when <string>] [force <yes or no=no>]
   cluster update name <string>

   clusterlog create eventid <string> [level <enum(critical|error|warning|info|debug)=info>]
   clusterlog show [count <int=20>] [excludeEvent <string>] [includeEvent <string>]

   condition show [display <enum(all|set|problem|flagged)=all>] [local <yes or no=no>] [filter <string=.*>] [order <enum(name|lanip|backplaneip|value|lastset|lastclear|validasof)=name>]

   configdata show type <string> [guid <string>]

   dns create serverlist <string> searchdomainlist <string>
   dns delete
   dns show
   dns update [serverlist <string>] [searchdomainlist <string>]

   drive show [node <node guid>] [order <enum(name|guid|slot|type|serial|currentdisp|desireddisp|rsd|errors)=name>]
   drive update guid <drive guid> desiredDisposition <enum(in|out)>

   event create name <string>

   ioperformancetest create driver <enum(vsd|rsd|raw)> [driverparameters <string>] [name <string>] [pattern <enum(sequential|random|spc1)=sequential>] [patternparameters <string>]
   ioperformancetest delete uuid <IO performance test uuid>
   ioperformancetest show [uuid <IO performance test uuid>] [output <enum(list|results|graph)=list>]

   logstream create daemon <enum(scaled|scribed)> outputpath <path> mask <logmask> [filters <string>]
   logstream show [daemon <enum(all|scaled|scribed)=all>]
   logstream update daemon <enum(scaled|scribed)> outputpath <path> mask <logmask> [filters <string>]
   logstream delete daemon <enum(scaled|scribed)> outputpath <path>

   node create othernode <x.x.x.x> [lan <x.x.x.x>] [backplane <x.x.x.x>] [rsdport <int=11003>]
   node delete guid <node guid>
   node reset
   node show [guid <node guid>] [order <enum(peerID|guid|backplane|lan)=peerID>]
   node update guid <node guid> oldip <x.x.x.x> lan <x.x.x.x> gateway <x.x.x.x> netmask <x.x.x.x>
   node exclude type <enum(vm)=vm> action <enum(start|stop)=start>
   node exclude-and-evacuate [type <enum(vm)=vm>] [file <string=/dev/stdout>] [wait <int=3600>]
   node export guid <node guid> type <enum(evacuationstrategy|evacuationstrategyinverse)=evacuationstrategy>

   registration update [companyName <string>] [contact <string>] [phone <string>] [email <string>]

   remoteconnection create ip <string> [useSecureConnection <yes or no=yes>] [useCompression <yes or no=yes>] [diagnostic <yes or no=no>]
   remoteconnection delete guid <remoteconnection guid>
   remoteconnection show [display <enum(list|ports|mappings)=list>] [guid <remoteconnection guid>]
   remoteconnection update guid <remoteconnection guid> compression <yes or no>

   replication show
   replication update enable <yes or no> [wait <yes or no=yes>]

   rollout create [wait <yes or no=yes>]

   rsd create devpath <path> [type <enum(unknown|sata|sas|ssd)=unknown>]
   rsd delete uuid <RSD UUID> [force <yes or no(no|yes)=no>]
   rsd discover path <string>
   rsd evacuate-and-delete ids <string>
   rsd evacuate id <int>
   rsd examine extentlist <string> [display <enum(all|diffs)=all>]
   rsd integrate id <int>
   rsd interpret extent <string>
   rsd offline id <int>
   rsd online id <int>
   rsd show [order <enum(id|instance|status|group|connection)=id>] [display <enum(list|tasks|frozenrefcounts)=list>] [reason <enum(any|invalidrefcount|mediaerror|reserved)=any>] [uuid <RSD UUID>]
   rsd aborttask uuid <RSD UUID> taskid <int> redirect <yes or no>
   rsd aborttask uuid <RSD UUID> taskid <int> redirect <yes or no>
   rsd update uuid <RSD UUID> [type <enum(sata|sas|ssd)>] [failuregroup <int>]

   rsdhash show

   rsdmetadata repair [id <int=-1>]
   rsdmetadata scan [id <int=-1>]

   rsdrefcount freeze extentlist <string> reason <enum(invalidrefcount|mediaerror|reserved)>
   rsdrefcount thaw [extentlist <string>] [refcount <int=0>]
   rsdrefcount update uuid <RSD UUID> extent <string> diff <int>

   scribeinstance show [filter <enum(all|local)=all>] [order <enum(peer|name|quorum|expiration|duration|local)=peer>]

   scriberuntime config name <Scribe runtime option> value <string>
   scriberuntime show

   scribestats show [format <enum(vertical|raw)=vertical>]

   scribetask show [order <enum(lane|id|status|type|state|detail)=lane>] [display <enum(tree|columns|list)=columns>] [descending <yes or no=yes>] [nodes <enum(local|all)=all>]

   smhistory show [region <state machine region>] [fromfile <path>] [interactive <yes or no=no>] [offset <int=0>]

   statemachine pause
   statemachine resume
   statemachine show [region <state machine region>] [allregions <yes or no=no>] [allstates <yes or no=no>]

   task delete tag <int>
   task show [display <enum(all|active)=all>] [count <int=20>] [ascending <yes or no=no>]
   task wait [count <int=100>]

   timesource create host <string>
   timesource delete
   timesource show
   timesource update host <string>

   timezone create zone <time zone>
   timezone delete
   timezone show
   timezone update zone <time zone>

   tunnel get port <int> ip <string> [cluster <yes or no=no>]
   tunnel add port <int> ip <string> [cluster <yes or no=no>] [useCompression <yes or no=no>] [sshPort <int=10022>]
   tunnel remove [port <int=0>] ip <string> [cluster <yes or no=no>]
   tunnel get port <int> ip <string> [cluster <yes or no=no>]

   user update name <string> password <string>

   vm clone guid <vm guid> [snapshot <string>] [name <string>] [description <string>] [vmBlockGUIDToCapacityList <string>] [cpuType <string>] [machineType <string>]
   vm create name <string> [guid <string>] [description <string=created by sctool>] [tags <string>] [os <enum(os_windows_2003_server_r2|os_windows_2008_server|os_windows_2008_server_r2|os_windows_server_2012|os_windows_7|os_windows_8|os_rhel_centos_5.5|os_rhel_centos_6|os_other)=os_rhel_centos_6>] [cpucount <int=2>] [ram <size=2GiB>]
   vm delete guid <vm guid>
   vm ssh guid <vm guid>
   vm export guid <vm guid> uri <string> [format <string>] [definition-file <string>] [compress <yes or no=no>] [snapshot-guid <string>] [name <string>] [description <string>] [wait <yes or no=yes>]
   vm import uri <string> definition-file <string> [wait <yes or no=yes>] [name <string>] [description <string>] [os <string>] [cpuType <string>]
   vm livemigrate guid <vm guid> node <node guid> [wait <int=3600>]
   vm importplacement [file <string=/dev/stdin>] [wait <int=3600>]
   vm show [display <enum(list|detail|snaps|xml|stats)=list>] [guid <vm guid>] [filter <enum(all|local|running|blocked|paused|shutdown|shutoff|crashed)=all>] [unavailable <yes or no=yes>] [orphaned <yes or no=yes>] [sourceGUID <string>] [order <enum(name|guid|mem|cpu|state|node|snaps)=name>] [withvsduuid <VSD UUID>]
   vm update guid <vm guid> [name <string>] [tags <string>] [cpus <int>] [ram <size>] [snapshotschedule <vm snapshot schedule guid>]
   vm start guid <vm guid>
   vm shutdown guid <vm guid>
   vm stop guid <vm guid>
   vm pause guid <vm guid>
   vm reboot guid <vm guid>
   vm reset guid <vm guid>

   vmblockdevice create vmguid <vm guid> [name <string>] [type <enum(virtio|ide|cdrom)=virtio>] [capacity <size=100GB>] [bootable <yes or no=no>] [cachemode <enum(none|writeback|writethrough)=writethrough>]
   vmblockdevice eject vmguid <vm guid> [guid <vmblockdevice cdromguid>] [type <enum(cdrom|floppy)>]
   vmblockdevice insert guid <vmblockdevice cdromguid> vsduuid <VSD UUID>
   vmblockdevice update vmguid <vm guid> [guid <vmblockdevice guid>] [disableSnapshotting <yes or no>]
   vmblockdevice show [vmguid <vm guid>]

   vmcputypes show

   vmexportedmedia refresh

   vmmachinetypes show

   vmnetdevice create vmguid <vm guid> [type <enum(virtio|realtek|intel)=virtio>] [mac <string>] [vlan <int=0>] [bootable <yes or no=no>] [connected <yes or no=yes>]

   vmreplication checksum [vmguid <vm guid>] [snapshot <int=0>] [useronly <yes or no=no>] [overwrite <yes or no=no>]
   vmreplication create vmGUID <vm guid> connectionGUID <remoteconnection guid> [enable <yes or no=yes>] [wait <yes or no=yes>] [label <string>]
   vmreplication delete guid <vm replication guid>
   vmreplication show [vmguid <vm guid>] [order <enum(connection|guid|source|target|enabled)=connection>] [display <enum(list|verification)=list>]
   vmreplication update [guid <vm replication guid>] [connectionGUID <remoteconnection guid>] enable <yes or no=yes> [label <string>] [wait <yes or no=yes>]

   vmsnapshot create guid <vm guid> label <string>
   vmsnapshot delete guid <vm snapshot guid>

   vmsnapshotschedule addrule guid <vm snapshot schedule guid> name <string> dtstart <string> retain <int> rrule <string>
   vmsnapshotschedule create name <string>
   vmsnapshotschedule deleterule guid <vm snapshot schedule guid> rruleguid <string>
   vmsnapshotschedule delete guid <vm snapshot schedule guid>
   vmsnapshotschedule show [guid <vm snapshot schedule guid>]
   vmsnapshotschedule update guid <vm snapshot schedule guid> [name <string>]

   vsd clone uuid <VSD UUID> snapshot <int> [capacity <int=0>]
   vsd create name <string> capacity <size> [replication <string=2>] [placement <enum(rr|freespace|capacity|ssdheat|ssdpreferred)=ssdheat>] [tier0priority <int=8>]
   vsd delete uuid <VSD UUID>
   vsd examine uuid <VSD UUID> extent <string>
   vsd find-reference extentlist <string> [uuid <VSD UUID>]
   vsd l1update uuid <VSD UUID> entry <int> replica <int> rsdaddress <string> reason <string>
   vsd mount uuid <VSD UUID> [params <string>]
   vsd panic uuid <VSD UUID> reason <string>
   vsd recover [uuid <VSD UUID>] [rebalance <enum(none|cluster|tier)=none>] [range <enum(all|snaps|metadata)=all>]
   vsd show [display <enum(list|detail|dump|history|layout|extentLayout|snaps|tasks|sessions|recovery|attachments|rsdlist|performance|heatmap)=list>] [uuid <VSD UUID>] [sourceUUID <string>] [order <enum(name|uuid|allocated|capacity|lease|status|reliability|availability|created)=name>] [snapshot <int=0>]
   vsd aborttask uuid <VSD UUID> taskid <int>
   vsd steptask uuid <VSD UUID> taskid <int>
   vsd unmount uuid <VSD UUID>
   vsd update uuid <VSD UUID> [sourceuuid <string>] [setflags <string>] [placement <enum(rr|freespace|capacity|ssdheat|ssdpreferred)>] [tier0Priority <int=8>] [tier0EnabledForInitialAllocations <yes or no=yes>] [tier0FixedCapacity <int=0>] [tier0MinHeat <int=1>]

   vsdchecksum show uuid <VSD UUID> [snapshot <int=0>] [offset <size=0>] [size <size=0>]

   vsdheatmap show [display <enum(data|params)=data>] [uuid <VSD UUID>] [type <enum(read|write|readwrite)=readwrite>] [edge <int=100>]
   vsdheatmap update [uuid <VSD UUID>] [maxHeat <int=-1>] [historyEntries <int=-1>] [triggerDelta <int=-1>] [triggerTier0Pop <int=-1>] [minTier0Pop <int=-1>] [initialHeatStart <int=-1>]

   vsdreplica check uuid <VSD UUID> [range <enum(head|snaps|all)=all>] [snaps <string=all>] [entries <string=all>] [depth <int=8>] [includeNonCorruptEntries <yes or no=no>] [printExactMismatches <yes or no=no>]
   vsdreplica show uuid <VSD UUID> [display <string(info|table)=info>] [snap <int=0>]

   vsdreplication create connectionGUID <remoteconnection guid> vsdUUID <VSD UUID> [enable <yes or no=yes>]
   vsdreplication delete uuid <vsd replication uuid>
   vsdreplication historydelete replicationUUID <vsd replication uuid>
   vsdreplication show [uuid <VSD UUID>] [order <enum(connection|uuid|source|target|enabled)=connection>] [limit <int=6>] [offset <int=0>] [ascending <yes or no=no>] [display <enum(list|detail|current|historical)=list>]
   vsdreplication skipitems replicationUUID <vsd replication uuid>
   vsdreplication update uuid <vsd replication uuid> [targetvsduuid <string>] [connectionGUID <remoteconnection guid>] [enable <yes or no>] [snapserialfrom <int>] [snapserialto <int>] [targetvsduuid <string>]

   vsdsnapdiff show uuid <VSD UUID> fromSerial <int> toSerial <int>

   vsdsnapshot create uuid <VSD UUID> [identifier <string>]
   vsdsnapshot delete uuid <VSD UUID> serial <int>

   zk showlocks [name <zk lock>] [filter <enum(none|locked)=none>]

