#!/usr/bin/env bash
# NAME: tcsetup.sh
#
# Created by:           jharris, mylon
# Created on:           6/02/2016
#
# CHANGELOG
# [username] - [date]     - [version] - [note]
# jsites     - 06/03/2016 - 1.1       - readability and clarification changes
#
#----------BEGIN SCRIPT----------

USAGE () {
    echo "Configure the maximum OUTBOUND kbit/s for port 100022 (replication)"
    echo "Enter the total kbit/s when prompted"
    echo "1000 kbit/s = 1 Mbit/s"
    echo ""
    echo "-h:"
    echo "--help:  This message"
    echo "-c:"
    echo "--clear: Clear previously set limits and not set new limits"
}

VERBOSEWAIT () {
# Waiting 5 seconds and displaying . each second
    TIMEOUT=5
    COUNTER=0
    while [ $COUNTER -lt $TIMEOUT ]
    do
        sleep 1
        echo -en "."
        COUNTER=$(($COUNTER+1))
    done
    echo -e "\n"
}

CLEAR () {
# Remove any existing TC policies
    echo "Traffic Control: Removing any existing policies..."
    /opt/scale/bin/scdoall "tc qdisc del dev lan0 root" && /opt/scale/bin/scdoall "tc qdisc del dev lan1 root" || return $?
    VERBOSEWAIT
    echo -e "Previously set TC policies removed\n"
}

case $1 in
    -h|--help)
        USAGE
        exit 1
        ;;
    -c|--clear)
        CLEAR
        exit $?
        ;;
esac

# Setting number of nodes to variable to calculate kbit/s limit per node
NODE=$(cat /opt/scale/var/nodes | wc -l)

# Gathering variable for desired total kbit/s for cluster-wide limits
echo -en "Enter the limit, in kbit, you wish to limit OUTBOUND replication from this cluster: "
read TOTAL
echo -e "\n"

# Calculate per-node limit to not exceed cluster-wide limit requested - only calculating to two decimal points
REPLIMIT=$(echo "scale = 2; $TOTAL/$NODE" | bc -l)

# Remove any existing policies
CLEAR

# Setting new policy
echo “Traffic Control: Setting new policy…”
VERBOSEWAIT

# Creating default rules
echo "Creating default rules"
echo -e "Adding replication limits to a maximum speed of: $REPLIMIT kbit/s per node\n"

/opt/scale/bin/scdoall "tc qdisc add dev lan0 root handle 2 htb default 1"
/opt/scale/bin/scdoall "tc qdisc add dev lan1 root handle 2 htb default 1"
/opt/scale/bin/scdoall "tc class add dev lan0 parent 2: classid 2:6 htb rate ${REPLIMIT}kbit"
/opt/scale/bin/scdoall "tc class add dev lan1 parent 2: classid 2:6 htb rate ${REPLIMIT}kbit"
/opt/scale/bin/scdoall "tc class add dev lan0 parent 2:6 classid 2:100d htb rate ${REPLIMIT}kbit"
/opt/scale/bin/scdoall "tc class add dev lan1 parent 2:6 classid 2:100d htb rate ${REPLIMIT}kbit"
/opt/scale/bin/scdoall "tc filter add dev lan0 parent 2: protocol ip pref 4 u32 match ip dport 10022 0xffff classid 2:100d"
/opt/scale/bin/scdoall "tc filter add dev lan1 parent 2: protocol ip pref 4 u32 match ip dport 10022 0xffff classid 2:100d"

echo -e "\nMaximum speed set.\nComplete"
exit 0
