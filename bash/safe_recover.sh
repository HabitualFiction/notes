#!/bin/bash

function is_safe {
	scdoall sc rsd show | grep IN | grep DISCONN || return 0
	return 1
}

function recover_degraded_vsds {
	for uuid_ip in $(sc vsd show | grep DEGRADED | awk '{printf("%s,%s\n",$1,$8)}'); do
		local uuid=${uuid_ip%_*}
		local ip=${uuid_ip#*_}
		rsh $ip sc vsd recover uuid $uuid
	done
}

is_safe || { \
	echo "There are disconnected RSDs; not safe to recover right now (we might be mid-upgrade on a node)."
	echo "The DEGRADED status might clear on its own."
	exit 1;
}

recover_degraded_vsds

echo "You might need to run scupdate resume if the upgrade is paused."