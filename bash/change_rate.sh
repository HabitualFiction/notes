#!/bin/bash

sc vm show display snaps | gawk '
/VM.GUID/ { if (d > 0) print vm,d; vm=$NF; d = 0; }
/^           Timestamp/ { match($0, / : (.*)$/, m); ts=m[1]; }
/^           Block Diff/ && ts >= "2018-03-14 14:00:00"  && $NF < 100000 { d += $NF }
END { if (d > 0) print vm,d; }
'